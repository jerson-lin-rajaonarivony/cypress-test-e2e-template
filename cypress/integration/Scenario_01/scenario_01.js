/// <reference types="cypress" />

import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"
import ConnexionPage from '../modules/Connexion.page';
import generateName from '../modules/Name.generator';

//OUVRIR LA PAGE DE CONNEXION
Given(`Je visite la page de connexion`, () => {
    //choix 1 : indiquer une donnée directement 
    cy.visit("https://staging-web.share.place/")

    //choix 2 : piocher une donnée dans le fichier data.json
    // cy.fixture('data.json').then(data => {
    //     cy.visit(data.connexion.page_Url)
    // })

    //choix 3 : utiliser une classe
    // ConnexionPage.ouvrirPage()
})
Then(`La page de connexion s'affiche`, () => {
    cy.get("img[alt=\"Share.Place visual connexion\"]", {timeout: 30000})
        .should('be.visible')
})

//SE CONNECTER
Given(`Je suis sur la page de connexion`, () => {
    cy.url().should('eq', 'https://staging-web.share.place/connexion/')
})
When(`Je saisis mon email dans le champ E-mail`, () => {
    cy.get("input[placeholder=\"E-mail\"]").type(`${generateName()}@yopmail.com`)  
})
    And(`Je saisis {string} dans le champ Mot de passe`, (valeur) => {
        cy.get("input[placeholder=\"Mot de passe\"]").type(valeur)  
    })
    And(`Je clique sur le bouton Se connecter`, () => {
        cy.get("#loginBtn").click()
        cy.wait(5000)
    })
Then(`Je suis connecté et je vois la page principale s'afficher`, () => {
    cy.get("div[data-cy=\"header-component\"]", {timeout: 60000})
        .get("div[data-cy=\"header-sidebar-component-btn-open\"]", {timeout: 30000})
        .contains('PLACES', {timeout: 30000}).should('be.visible')
    cy.get("div[data-cy=\"header-userprofil-component-btn-open\"]", {timeout: 30000}).should('be.visible')
    cy.wait(5000)
})