export default class ConnexionPage {

    static ouvrirPage () {
        cy.fixture('data.json').then(data => {
            cy.visit(data.connexion.page_Url)
        })
    }

    static verificationPageConnexion() {
        cy.fixture('data.json').then(data => {
            cy.get(data.connexion.image_SharePlace, {timeout: 30000})
                .should('be.visible')
        })
    }

    static saisirEmail(valeur) {
        cy.fixture('data.json').then(data => {
            cy.get(data.connexion.email_Input).type(valeur)  
        })
    }

    static saisirMotDePasse(valeur) {
        cy.fixture('data.json').then(data => {
            cy.get(data.connexion.password_Input).type(valeur)   
        })
    }

    static seConnecter(){
        cy.fixture('data.json').then(data => {
            cy.contains(data.connexion.login_Button).click()
            cy.wait(5000)
        })
    }

    static verificationPagePrincipale() {
        cy.fixture('data.json').then(data => {
            cy.get(data.header.bar, {timeout: 60000})
                .get(data.header.place, {timeout: 30000})
                .contains('PLACES', {timeout: 30000}).should('be.visible')
            cy.get(data.header.userprofile, {timeout: 30000}).should('be.visible')
        })
        cy.wait(5000)
    }

}