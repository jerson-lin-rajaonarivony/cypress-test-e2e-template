const report = require('cucumber-html-reporter')

const options = {
    theme: 'bootstrap',
    jsonDir: 'cypress/cucumber-json',
    output: 'cypress/reports/cucumber-html-report/index.html',
    reportSuiteAsScenarios: true, //Reports total number of passed/failed scenarios
    scenarioTimestamp: true,
    ignoredBadJsonFile: true,
    launchReport: false, //Automatically launch HTML report at the end of test suite in default browser

    metadata: {
        "Project": "Nom du projet",
        "Test Environment": "Environnement",
        "Browser": "Version navigateur",
        "Platform": "Système d'exploitation",
        "Parallel": "Scenarios",
        "Executed": "Remote"
    }
}

report.generate(options)